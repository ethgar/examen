<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class UsuarioAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $usuario = User::create([
            'name' => 'USUARIO ADMIN',
            'email' => 'admin@mail.com',
            'password' => Hash::make('admin')
        ]);
        $usuario->syncRoles(['admin']);


    }
}
