<?php

namespace Modules\Administracion\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $usuarios = User::all();
        return view('administracion::usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $roles = Role::all();
        return view('administracion::usuarios.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
           'name'     => ['required'],
           'email'    => ['required', 'unique:users'],
           'password' => ['required', 'confirmed'],
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $usuario = new User();
        $usuario->name      = $request->name;
        $usuario->email     = $request->email;
        $usuario->password  = Hash::make($request->password);
        $usuario->save();
        $usuario->refresh();

        if($request->roles){
            $usuario->syncRoles($request->roles);
        }

        return redirect('administracion/usuarios')->with('estatus', 'ok')->with('mensaje', 'Usuario creado correctamente');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('administracion::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $usuario    = User::find($id);
        $roles      = Role::all();
        return view('administracion::usuarios.edit', compact('usuario', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'email'    => ['required'],
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $usuario = User::find($id);
        $usuario->name  = $request->name;
        $usuario->email = $request->email;
        $usuario->save();
        $usuario->refresh();

        return redirect('administracion/usuarios')->with('estatus', 'ok')->with('mensaje', 'Usuario actualizado correctamente');
    }

    public function updatePassword(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'password'     => ['required', 'confirmed']
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $usuario = User::find($id);
        $usuario->password  = Hash::make($request->pasword);
        $usuario->save();
        return redirect('administracion/usuarios')->with('estatus', 'ok')->with('mensaje', 'Contraseña actualizada correctamente');
    }

    public function updateRoles(Request $request, $id){
        $usuario = User::find($id);
        $usuario->syncRoles($request->roles);
        return redirect('administracion/usuarios')->with('estatus', 'ok')->with('mensaje', 'Roles actualizados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        User::destroy($id);
        return redirect('administracion/usuarios')->with('estatus', 'ok')->with('mensaje', 'Usuario eliminado correctamente');
    }
}
