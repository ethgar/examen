<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'accesrole:admin'])->prefix('administracion')->group(function() {

    # Rutas para administración de usuarios
    Route::get('/usuarios', 'UsuariosController@index')->name('administracion.usuarios');
    Route::get('/usuarios-agregar', 'UsuariosController@create')->name('administracion.usuarios.create');
    Route::post('/usuarios-guardar', 'UsuariosController@store')->name('administracion.usuarios.store');
    Route::get('/usuarios-editar/{id}', 'UsuariosController@edit')->name('administracion.usuarios.edit');
    Route::post('/usuarios-actualizar/{id}', 'UsuariosController@update')->name('administracion.usuarios.update');
    Route::post('/usuarios-actualizar-contrasena/{id}', 'UsuariosController@updatePassword')->name('administracion.usuarios.update.password');
    Route::post('/usuarios-actualizar-roles/{id}', 'UsuariosController@updateRoles')->name('administracion.usuarios.update.roles');
    Route::get('/usuarios-eliminar/{id}', 'UsuariosController@destroy')->name('administracion.usuarios.destroy');
});
