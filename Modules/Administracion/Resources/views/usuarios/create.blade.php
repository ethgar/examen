@extends('layouts.app')

@section('content')

    <div class="container">
        <nav  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('administracion.usuarios') }}">Usuarios</a></li>
                <li class="breadcrumb-item active" aria-current="page">Agregar</li>
            </ol>
        </nav>
    </div>

    <div class="container">

        <div class="row">

            <form action="{{ route('administracion.usuarios.store') }}" method="POST">
                @csrf
                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            Información del usuario
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="nombre" name="name" value="{{ old('name') }}">
                                @error('name')
                                    <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="nombreInfo" class="form-text text-muted">Ingrese el nombre del usuario.</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
                                @error('email')
                                    <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="emailInfo" class="form-text text-muted">Ingrese el correo electrónico del usuario.</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                                @error('password')
                                    <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="passwordInfo" class="form-text text-muted">Ingrese la contraseña del usuario.</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="confirm_password">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="confirm_password" name="password_confirmation">
                                <small id="confirmpasswordInfo" class="form-text text-muted">Confirme la contraseña.</small>
                            </div>

                            <div class="form-group">
                                <div class="card-body">
                                    @foreach($roles as $rol)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="roles" value="{{ $rol->name }}" id="check-{{ $rol->name }}">
                                            <label class="form-check-label" for="check-{{ $rol->name }}">
                                                {{ $rol->name }}
                                            </label>
                                        </div>
                                    @endforeach

                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary m" value="Guardar" >
                                <input type="reset" class="btn btn-secondary" value="Cancelar" >
                            </div>


                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
