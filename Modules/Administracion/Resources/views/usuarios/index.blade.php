@extends('layouts.app')

@section('content')

    <div class="container">
        <nav  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
            </ol>
        </nav>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div class="card">
                    <div class="card-header">
                        Lista de Usuarios
                        <a href="{{ route('administracion.usuarios.create') }}" class="btn btn-primary btn-sm" style="float: right;">Nuevo Usuario</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered data-table">
                            <thead>
                            <tr>
                                <th width="50">Id</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Opciónes</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{ $usuario->id }}</td>
                                    <td>{{ $usuario->name }}</td>
                                    <td>{{ $usuario->email }}</td>
                                    <td>
                                        <a href="{{ route('administracion.usuarios.edit', ['id' => $usuario->id]) }}" class="btn btn-warning btn-sm">Editar</a>
                                        <a href="{{ route('administracion.usuarios.destroy', ['id' => $usuario->id]) }}" class="btn btn-danger btn-sm">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

