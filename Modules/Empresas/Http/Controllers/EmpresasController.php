<?php

namespace Modules\Empresas\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Empresas\Entities\Companies;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $empresas = Companies::all();
        return view('empresas::empresas.index', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('empresas::empresas.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'  => ['required', 'unique:companies'],
            'email' => ['required', 'unique:companies'],
            'logo'  => ['required', 'dimensions:min_width=150,min_height=150']
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if($request->logo){
            $file_name = $request->logo->hashName();

            $request->logo->store(
                'empresas',
                'public'
            );
        }
        $empresa = new Companies();
        $empresa->name      = $request->name;
        $empresa->email     = $request->email;
        $empresa->website   = $request->website;
        $empresa->logo      = $file_name;
        $empresa->id_user   = Auth::user()->id;
        $empresa->save();
        $empresa->refresh();

        return redirect('empresas')->with('estatus', 'ok')->with('mensaje', 'Empresa creada correctamente');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('empresas::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $empresa = Companies::find($id);
        return view('empresas::empresas.edit', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'  => ['required'],
            'email' => ['required'],
            'logo'  => ['dimensions:min_width=150,min_height=150']
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $empresa = Companies::find($id);
        if($request->logo){
            $file_name = $request->logo->hashName();

            $request->logo->store(
                'empresas',
                'public'
            );
            Storage::disk('public')->delete('empresas/'.$empresa->logo);
        }

        $empresa->name      = $request->name;
        $empresa->email     = $request->email;
        $empresa->website   = $request->website;
        ($request->logo)    ? $empresa->logo      = $file_name : '';
        $empresa->update();
        $empresa->refresh();

        return redirect('empresas')->with('estatus', 'ok')->with('mensaje', 'Empresa actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $empresa = Companies::find($id);
        if($empresa->empleados->count() > 0){
            return redirect('empresas')->with('estatus', 'fail')->with('mensaje', 'La empresa no puede ser eliminada hasta que elimine los usuarios en ella.');
        }else{
            Storage::disk('public')->delete('empresas/'.$empresa->logo);
            $empresa->delete();
            return redirect('empresas')->with('estatus', 'ok')->with('mensaje', 'Empresa eliminada correctamente');
        }
    }
}
