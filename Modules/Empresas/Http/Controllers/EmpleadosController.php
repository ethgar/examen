<?php

namespace Modules\Empresas\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Empresas\Entities\Companies;
use Modules\Empresas\Entities\Employees;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($id)
    {
        $empresa = Companies::find($id);
        return view('empresas::empleados.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($id)
    {
        $empresa = Companies::find($id);
        return view('empresas::empleados.create', compact('empresa'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'      => ['required'],
            'last_name' => ['required'],
            'email'     => ['required', 'unique:employees'],
            'phone'     => ['required']
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $buscar_empleado = Employees::where('name', $request->name)->where('last_name', $request->last_name)->first();
        if(isset($buscar_empleado->name)){
            return redirect('empresas/empleados/'.$buscar_empleado->companie->id)->with('estatus', 'fail')->with('mensaje', 'El empleado ya se encuentra registrado en la empresa '.$buscar_empleado->companie->name);
        }

        $empleado = new Employees();
        $empleado->name = $request->name;
        $empleado->last_name = $request->last_name;
        $empleado->email = $request->email;
        $empleado->phone = $request->phone;
        $empleado->id_company = $id;
        $empleado->id_user = Auth::user()->id;
        $empleado->save();
        return redirect('empresas/empleados/'.$id)->with('estatus', 'ok')->with('mensaje', 'Empleado creado correctamente');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('empresas::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id_empleado)
    {
        $empleado = Employees::find($id_empleado);
        return view('empresas::empleados.edit', compact('empleado'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'      => ['required'],
            'last_name' => ['required'],
            'email'     => ['required', 'unique:employees'],
            'phone'     => ['required']
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $buscar_empleado = Employees::where('name', $request->name)->where('last_name', $request->last_name)->whereNotIn('id', [$id])->first();
        if(isset($buscar_empleado->name)){
            return redirect('empresas/empleados/'.$buscar_empleado->companie->id)->with('estatus', 'fail')->with('mensaje', 'El empleado ya se encuentra registrado en la empresa '.$buscar_empleado->companie->name);
        }

        $empleado = Employees::find($id);
        $empleado->name         = $request->name;
        $empleado->last_name    = $request->last_name;
        $empleado->email        = $request->email;
        $empleado->phone        = $request->phone;
        $empleado->update();
        return redirect('empresas/empleados/'.$empleado->id_company)->with('estatus', 'ok')->with('mensaje', 'Empleado actualizado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $empleado = Employees::find($id);
        $empresa_id = $empleado->id_company;
        $empleado->delete();
        return redirect('empresas/empleados/'.$empresa_id)->with('estatus', 'ok')->with('mensaje', 'Empleado eliminado correctamente');
    }
}
