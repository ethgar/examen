<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->prefix('empresas')->group(function() {

    #Rutas para CRUD empresas
    Route::get('/', 'EmpresasController@index')->name('empresas');
    Route::middleware('accesrole:admin')->get('/agregar', 'EmpresasController@create')->name('empresas.agregar');
    Route::middleware('accesrole:admin')->post('/guardar', 'EmpresasController@store')->name('empresas.store');
    Route::middleware('accesrole:admin')->get('/editar/{id}', 'EmpresasController@edit')->name('empresas.edit');
    Route::middleware('accesrole:admin')->post('/actualizar/{id}', 'EmpresasController@update')->name('empresas.update');
    Route::middleware('accesrole:admin')->get('/eliminar/{id}', 'EmpresasController@destroy')->name('empresas.destroy');

    #Rutas para CRUD de empleados de empresas
    Route::get('empleados/{id}', 'EmpleadosController@index')->name('empresas.empleados');
    Route::middleware('accesrole:admin')->get('empleados/agregar/{id}', 'EmpleadosController@create')->name('empresas.empleados.create');
    Route::middleware('accesrole:admin')->post('empleados/guardar/{id}', 'EmpleadosController@store')->name('empresas.empleados.store');
    Route::middleware('accesrole:admin')->get('empleados/editar/{id}', 'EmpleadosController@edit')->name('empresas.empleados.edit');
    Route::middleware('accesrole:admin')->post('empleados/actualizar/{id}', 'EmpleadosController@update')->name('empresas.empleados.update');
    Route::middleware('accesrole:admin')->get('empleados/eliminar/{id}', 'EmpleadosController@destroy')->name('empresas.empleados.destroy');
});
