@extends('layouts.app')

@section('content')

    <div class="container">
        <nav  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('empresas') }}">Empresas</a></li>
                <li class="breadcrumb-item active" aria-current="page">Agregar</li>
            </ol>
        </nav>
    </div>

    <div class="container">

        <div class="row">

            <form action="{{ route('empresas.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            Información de la empresa
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="logo">Logo</label>
                                <div class="input-group">
                                    <input type="file" class="form-control @error('logo') is-invalid @enderror" id="logo" name="logo" accept="image/png,image/jpeg,image/jpg">
                                </div>
                                @error('logo')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="logoInfo" class="form-text text-muted">Ingrese el nombre de la empresa (solo se aceptan formatos png, jpg y jpeg y con un tamaño mayor a 150x150).</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="nombre" name="name" value="{{ old('name') }}">
                                @error('name')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="nombreInfo" class="form-text text-muted">Ingrese el nombre de la empresa.</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
                                @error('email')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="emailInfo" class="form-text text-muted">Ingrese el correo electrónico de la empresa.</small>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="text" class="form-control @error('website') is-invalid @enderror" id="website" name="website" value="{{ old('website') }}">
                                @error('website')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="emailInfo" class="form-text text-muted">Ingrese el sitio web de la empresa.</small>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary m" value="Guardar" >
                                <input type="reset" class="btn btn-secondary" value="Cancelar" >
                            </div>


                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
