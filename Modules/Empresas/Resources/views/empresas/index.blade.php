@extends('layouts.app')

@section('content')

    <div class="container">
        <nav  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Empresas</li>
            </ol>
        </nav>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div class="card">
                    <div class="card-header">
                        Lista de Empresas
                        @if(Auth::user()->hasRole('admin'))
                            <a href="{{ route('empresas.agregar') }}" class="btn btn-primary btn-sm" style="float: right;">Nueva Empresa</a>
                        @endif
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered data-table">
                            <thead>
                            <tr>
                                <th width="50">Logo</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Website</th>
                                <th>Empleados</th>
                                <th>Opciónes</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($empresas as $empresa)
                                <tr>
                                    <td><img src="{{ asset('storage/empresas').'/'.$empresa->logo }}" alt="" width="60"></td>
                                    <td>{{ $empresa->name }}</td>
                                    <td>{{ $empresa->email }}</td>
                                    <td>{{ $empresa->website }}</td>
                                    <td>{{ $empresa->empleados->count() }}</td>
                                    <td>

                                        <a href="{{ route('empresas.empleados', ['id' => $empresa->id]) }}" class="btn btn-primary btn-sm">Empleados</a>
                                        @if(Auth::user()->hasRole('admin'))
                                            <a href="{{ route('empresas.edit', ['id' => $empresa->id]) }}" class="btn btn-warning btn-sm">Editar</a>
                                            <a href="{{ route('empresas.destroy', ['id' => $empresa->id]) }}" class="btn btn-danger btn-sm">Eliminar</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

