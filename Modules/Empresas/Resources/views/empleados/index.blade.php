@extends('layouts.app')

@section('content')

    <div class="container">
        <nav  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('empresas') }}">Empresas</a></li>
                <li class="breadcrumb-item active" aria-current="page">Empleados de {{ $empresa->name }}</li>
            </ol>
        </nav>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-12">

                <div class="card">
                    <div class="card-header">
                        Lista de Empresas
                        @if(Auth::user()->hasRole('admin'))
                            <a href="{{ route('empresas.empleados.create', ['id' => $empresa->id]) }}" class="btn btn-primary btn-sm" style="float: right;">Nuevo Empleado</a>
                        @endif
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered data-table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Teléfono</th>
                                <th>Opciónes</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($empresa->empleados as $empleado)
                                <tr>
                                    <td>{{ $empleado->name }} {{ $empleado->last_name }}</td>
                                    <td>{{ $empleado->email }}</td>
                                    <td>{{ $empleado->phone }}</td>
                                    <td>
                                        @if(Auth::user()->hasRole('admin'))
                                            <a href="{{ route('empresas.empleados.edit', ['id' => $empleado->id]) }}" class="btn btn-warning btn-sm">Editar</a>
                                            <a href="{{ route('empresas.empleados.destroy', ['id' => $empleado->id]) }}" class="btn btn-danger btn-sm">Eliminar</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

