@extends('layouts.app')

@section('content')

    <div class="container">
        <nav  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('empresas') }}">Empresas</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('empresas.empleados', ['id' => $empleado->companie->id]) }}">Empleados de {{ $empleado->companie->name }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar</li>
            </ol>
        </nav>
    </div>

    <div class="container">

        <div class="row">

            <form action="{{ route('empresas.empleados.update', ['id' => $empleado->id]) }}" method="POST">
                @csrf
                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            Información del empleado
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="nombre" name="name" value="{{ $empleado->name }}">
                                @error('name')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="nombreInfo" class="form-text text-muted">Ingrese el nombre del empleado.</small>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="lastname">Apellidos</label>
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" id="lastname" name="last_name" value="{{ $empleado->last_name }}">
                                @error('lastname')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="nombreInfo" class="form-text text-muted">Ingrese los apellidos del empleado.</small>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ $empleado->email }}">
                                @error('email')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="emailInfo" class="form-text text-muted">Ingrese el correo electrónico de la empresa.</small>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <label for="phone">Teléfono</label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ $empleado->phone }}">
                                @error('phone')
                                <span class="invalid-feedback" alert="role">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @else
                                    <small id="nombreInfo" class="form-text text-muted">Ingrese los apellidos del empleado.</small>
                                    @enderror
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary m" value="Guardar" >
                                <input type="reset" class="btn btn-secondary" value="Cancelar" >
                            </div>


                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
