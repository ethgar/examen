<?php

namespace Modules\Empresas\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Empresas\Entities\Companies;
class Employees extends Model
{

    protected $table = 'employees';
    protected $fillable = [];

    public function companie(){
        return $this->belongsTo(Companies::class, 'id_company');
    }

}
