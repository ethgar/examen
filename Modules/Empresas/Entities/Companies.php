<?php

namespace Modules\Empresas\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Empresas\Entities\Employees;

class Companies extends Model
{

    protected $table = 'companies';
    protected $fillable = [];

    public function empleados(){
        return $this->hasMany(Employees::class, 'id_company');
    }

}
